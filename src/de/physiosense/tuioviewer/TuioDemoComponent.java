/*******************************************************************
 * Copyright (c) 2018 physiosense GmbH
 * 
 * This project or parts of it can not be copied and/or distributed 
 * without the express permission of physiosense GmbH
 * 
 * Proprietary and confidential
 * Written by Philipp Arnolds <arnolds@physiosense.de>, April 2018
 *******************************************************************/

package de.physiosense.tuioviewer;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;

import javax.swing.JComponent;

import tuio.TuioBlob;
import tuio.TuioCursor;
import tuio.TuioListener;
import tuio.TuioObject;
import tuio.TuioPoint;
import tuio.TuioTime;

public class TuioDemoComponent extends JComponent implements TuioListener {

	private static final long serialVersionUID = 1L;
	private Hashtable<Long, TuioObject> objectList = new Hashtable<Long, TuioObject>();
	private Hashtable<Long, TuioCursor> cursorList = new Hashtable<Long, TuioCursor>();
	private Hashtable<Long, TuioBlob> blobList = new Hashtable<Long, TuioBlob>();

	public static final int finger_size = 18;
	public static final int object_size = 60;
	public static final int table_size = 760;

	public static int width, height;
	private float scale = 1.0f;
	public boolean verbose = false;

	Color bgrColor = new Color(0, 0, 64);
	Color curColor = new Color(192, 0, 192);
	Color objColor = new Color(64, 0, 0);
	Color blbColor = new Color(64, 64, 64);

	public void setSize(int w, int h) {
		super.setSize(w, h);
		width = w;
		height = h;
		scale = height / (float) TuioDemoComponent.table_size;
	}

	public void addTuioCursor(TuioCursor tcur) {

		cursorList.put(tcur.getSessionID(), tcur);

		if (verbose)
			System.out.println("add cur " + tcur.getCursorID() + " (" + tcur.getSessionID() + ") " + tcur.getX() + " "
					+ tcur.getY());
	}

	public void updateTuioCursor(TuioCursor tcur) {

		if (verbose)
			System.out.println("set cur " + tcur.getCursorID() + " (" + tcur.getSessionID() + ") " + tcur.getX() + " "
					+ tcur.getY() + " " + tcur.getMotionSpeed() + " " + tcur.getMotionAccel());
	}

	public void removeTuioCursor(TuioCursor tcur) {

		cursorList.remove(tcur.getSessionID());

		if (verbose)
			System.out.println("del cur " + tcur.getCursorID() + " (" + tcur.getSessionID() + ")");
	}

	public void addTuioObject(TuioObject tobj) {
		objectList.put(tobj.getSessionID(), tobj);

		if (verbose)
			System.out.println("add obj " + tobj.getSymbolID() + " (" + tobj.getSessionID() + ") " + tobj.getX() + " "
					+ tobj.getY() + " " + tobj.getAngle());
	}

	public void updateTuioObject(TuioObject tobj) {

		if (verbose)
			System.out.println("set obj " + tobj.getSymbolID() + " (" + tobj.getSessionID() + ") " + tobj.getX() + " "
					+ tobj.getY() + " " + tobj.getAngle() + " " + tobj.getMotionSpeed() + " " + tobj.getRotationSpeed()
					+ " " + tobj.getMotionAccel() + " " + tobj.getRotationAccel());
	}

	public void removeTuioObject(TuioObject tobj) {
		objectList.remove(tobj.getSessionID());

		if (verbose)
			System.out.println("del obj " + tobj.getSymbolID() + " (" + tobj.getSessionID() + ")");
	}

	public void addTuioBlob(TuioBlob tblb) {
		blobList.put(tblb.getSessionID(), tblb);

		if (verbose)
			System.out.println("add blb " + tblb.getBlobID() + " (" + tblb.getSessionID() + ") " + tblb.getX() + " "
					+ tblb.getY() + " " + tblb.getAngle());
	}

	public void updateTuioBlob(TuioBlob tblb) {

		if (verbose)
			System.out.println("set blb " + tblb.getBlobID() + " (" + tblb.getSessionID() + ") " + tblb.getX() + " "
					+ tblb.getY() + " " + tblb.getAngle() + " " + tblb.getMotionSpeed() + " " + tblb.getRotationSpeed()
					+ " " + tblb.getMotionAccel() + " " + tblb.getRotationAccel());
	}

	public void removeTuioBlob(TuioBlob tblb) {
		blobList.remove(tblb.getSessionID());

		if (verbose)
			System.out.println("del blb " + tblb.getBlobID() + " (" + tblb.getSessionID() + ")");
	}

	public void refresh(TuioTime frameTime) {
		repaint();
	}

	public void paint(Graphics g) {
		update(g);
	}

	public void update(Graphics g) {

		Graphics2D g2 = (Graphics2D) g;
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);

		g2.setColor(bgrColor);
		g2.fillRect(0, 0, width, height);

		int w = (int) Math.round(width - scale * finger_size / 2.0f);
		int h = (int) Math.round(height - scale * finger_size / 2.0f);

		Enumeration<TuioCursor> cursors = cursorList.elements();
		while (cursors.hasMoreElements()) {
			TuioCursor tcur = cursors.nextElement();
			if (tcur == null)
				continue;
			ArrayList<TuioPoint> path = tcur.getPath();
			TuioPoint current_point = path.get(0);
			if (current_point != null) {
				// draw the cursor path
				g2.setPaint(Color.blue);
				for (int i = 0; i < path.size(); i++) {
					TuioPoint next_point = path.get(i);
					g2.drawLine(current_point.getScreenX(w), current_point.getScreenY(h), next_point.getScreenX(w),
							next_point.getScreenY(h));
					current_point = next_point;
				}
			}

			// draw the finger tip
			g2.setPaint(curColor);
			int s = (int) (scale * finger_size);
			g2.fillOval(current_point.getScreenX(w - s / 2), current_point.getScreenY(h - s / 2), s, s);
			g2.setPaint(Color.white);
			g2.drawString("tID:" + tcur.getCursorID() + "", current_point.getScreenX(w) + 10,
					current_point.getScreenY(h) - 10);
			g2.drawString("sID:" + tcur.getSessionID() + "", current_point.getScreenX(w) + 10,
					current_point.getScreenY(h) + 10);
		}

		// draw the objects
		Enumeration<TuioObject> objects = objectList.elements();
		while (objects.hasMoreElements()) {
			TuioObject tobj = objects.nextElement();
			if (tobj != null) {

				float ox = tobj.getScreenX(width);
				float oy = tobj.getScreenY(height);
				float size = object_size * (height / (float) table_size);

				Rectangle2D square = new Rectangle2D.Float(-size / 2, -size / 2, size, size);

				AffineTransform transform = new AffineTransform();
				transform.rotate(tobj.getAngle(), ox, oy);
				transform.translate(ox, oy);

				g2.setPaint(objColor);
				g2.fill(transform.createTransformedShape(square));
				g2.setPaint(Color.white);
				g2.drawString(tobj.getSymbolID() + "", ox - 10, oy);
			}
		}

		// draw the blobs
		Enumeration<TuioBlob> blobs = blobList.elements();
		while (blobs.hasMoreElements()) {
			TuioBlob tblb = blobs.nextElement();
			if (tblb != null) {

				float bx = tblb.getScreenX(width);
				float by = tblb.getScreenY(height);
				float bw = tblb.getScreenWidth(width);
				float bh = tblb.getScreenHeight(height);
				Ellipse2D ellipse = new Ellipse2D.Float(-bw / 2.0f, -bh / 2.0f, bw, bh);

				AffineTransform transform = new AffineTransform();
				transform.rotate(tblb.getAngle(), bx, by);
				transform.translate(bx, by);

				g2.setPaint(blbColor);
				g2.fill(transform.createTransformedShape(ellipse));
				g2.setPaint(Color.white);
				g2.drawString(tblb.getBlobID() + "", bx - 10, by);
			}
		}
	}
}
