/*******************************************************************
 * Copyright (c) 2018 physiosense GmbH
 * 
 * This project or parts of it can not be copied and/or distributed 
 * without the express permission of physiosense GmbH
 * 
 * Proprietary and confidential
 * Written by Philipp Arnolds <arnolds@physiosense.de>, April 2018
 *******************************************************************/

package de.physiosense.tuioviewer;

import java.awt.Cursor;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JFrame;

import de.physiosense.mouse.MouseGUI;
import tuio.TuioClient;
import tuio.TuioListener;

public class TuioViewer {

	private final int window_width = 600;
	private final int window_height = 600;

	private boolean fullscreen = false;

	private TuioDemoComponent demo;
	private JFrame frame;
	private GraphicsDevice device;
	private Cursor invisibleCursor;

	public TuioViewer() {
		demo = new TuioDemoComponent();
		device = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
		invisibleCursor = Toolkit.getDefaultToolkit().createCustomCursor(
				new BufferedImage(16, 16, BufferedImage.TYPE_INT_ARGB), new Point(0, 0), "invisible cursor");
		setupWindow();
		showWindow();
	}

	public TuioListener getTuioListener() {
		return demo;
	}

	public void setupWindow() {

		frame = new JFrame();
		frame.add(demo);
		frame.setTitle("physiosense Touch View");
		frame.setResizable(false);
		URL iconURL = MouseGUI.class.getResource("images/48icon.png");
		ImageIcon icon = new ImageIcon(iconURL);
		frame.setIconImage(icon.getImage());
		frame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent evt) {
				System.exit(0);
			}
		});

		frame.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent evt) {
				if (evt.getKeyCode() == KeyEvent.VK_ESCAPE)
					System.exit(0);
				else if (evt.getKeyCode() == KeyEvent.VK_F1) {
					destroyWindow();
					setupWindow();
					fullscreen = !fullscreen;
					showWindow();
				} else if (evt.getKeyCode() == KeyEvent.VK_V)
					demo.verbose = !demo.verbose;
			}
		});
	}

	public void destroyWindow() {

		frame.setVisible(false);
		if (fullscreen) {
			device.setFullScreenWindow(null);
		}
		frame = null;
	}

	public void showWindow() {

		if (fullscreen) {
			int width = (int) Toolkit.getDefaultToolkit().getScreenSize().getWidth();
			int height = (int) Toolkit.getDefaultToolkit().getScreenSize().getHeight();
			demo.setSize(width, height);

			frame.setSize(width, height);
			frame.setUndecorated(true);
			device.setFullScreenWindow(frame);
			frame.setCursor(invisibleCursor);
		} else {
			int width = window_width;
			int height = window_height;
			demo.setSize(width, height);

			frame.pack();
			Insets insets = frame.getInsets();
			frame.setSize(width, height + insets.top);
			frame.setCursor(Cursor.getDefaultCursor());
		}

		frame.setVisible(true);
		frame.repaint();
	}

	public static void main(String argv[]) {

		TuioViewer demo = new TuioViewer();
		TuioClient client = null;

		switch (argv.length) {
		case 1:
			try {
				client = new TuioClient(Integer.parseInt(argv[0]));
			} catch (Exception e) {
				System.out.println("usage: java TuioDemo [port]");
				System.exit(0);
			}
			break;
		case 0:
			client = new TuioClient();
			break;
		default:
			System.out.println("usage: java TuioDemo [port]");
			System.exit(0);
			break;
		}

		if (client != null) {
			client.addTuioListener(demo.getTuioListener());
			client.connect();
		} else {
			System.out.println("usage: java TuioDemo [port]");
			System.exit(0);
		}
	}

}
