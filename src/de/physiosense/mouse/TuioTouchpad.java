/*******************************************************************
 * Copyright (c) 2018 physiosense GmbH
 * 
 * This project or parts of it can not be copied and/or distributed 
 * without the express permission of physiosense GmbH
 * 
 * Proprietary and confidential
 * Written by Philipp Arnolds <arnolds@physiosense.de>, April 2018
 *******************************************************************/

package de.physiosense.mouse;

import java.awt.Desktop;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.event.InputEvent;
import java.net.URI;

import tuio.TuioBlob;
import tuio.TuioClient;
import tuio.TuioCursor;
import tuio.TuioListener;
import tuio.TuioObject;
import tuio.TuioTime;

public class TuioTouchpad implements TuioListener {

	private Robot robot = null;
	private int width = 0;
	private int height = 0;
	private long mouse = -1;
	private boolean scrolling;
	private static boolean reverse = false;
	private static int scrollSpeed = 5;
	public static String url1 = "http://google.de";
	public static String url2 = "http://google.de";
	public static float speedMultiplicator = 1.000f;
	private static TuioClient client;
	private static TuioTouchpad trackpad;

	public void addTuioCursor(TuioCursor tuioCursor) {

		Point pos = MouseInfo.getPointerInfo().getLocation();

		int xpos = pos.x + (int) Math.round(tuioCursor.getXSpeed() * Math.sqrt(width));

		if (xpos < 0)
			xpos = 0;
		if (xpos > width)
			xpos = width;
		int ypos = pos.y + (int) Math.round(tuioCursor.getYSpeed() * Math.sqrt(height));
		if (ypos < 0)
			ypos = 0;
		if (ypos > height)
			ypos = height;
		if (mouse < 0) {
			mouse = tuioCursor.getSessionID();
			if (robot != null) {
				robot.mouseMove(xpos, ypos);
				scrolling = false;
			}
		} else {
			// two finger gesture
			if (robot != null) {
				scrolling = true;
			}
		}

		// top left corner key
		if (tuioCursor.getY() <= 0.1 && tuioCursor.getX() <= 0.1) {
			openWebsite(url1);
		}

		// top right key
		if (tuioCursor.getY() <= 0.1 && tuioCursor.getX() > 0.9) {
			openWebsite(url2);
		}

		if (tuioCursor.getY() > 0.9) {
			// trigger left click
			if (tuioCursor.getX() <= 0.5) {
				System.out.println("Left click");
				if (robot != null)
					robot.mousePress(InputEvent.BUTTON1_MASK);
				robot.mouseRelease(InputEvent.BUTTON1_MASK);
				// trigger right click
			} else if (tuioCursor.getX() > 0.5) {
				System.out.println("Right click");
				if (robot != null)
					robot.mousePress(InputEvent.BUTTON3_MASK);
				robot.mouseRelease(InputEvent.BUTTON3_MASK);

			}
		}
	}

	public void openWebsite(String url) {
		try {
			Desktop desktop = java.awt.Desktop.getDesktop();
			URI oURL = new URI(url);
			System.out.println("Opening Browser: " + url);
			desktop.browse(oURL);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void updateTuioCursor(TuioCursor tcur) {
		Point pos = MouseInfo.getPointerInfo().getLocation();
		int xpos = pos.x + (int) Math.round(tcur.getXSpeed() * speedMultiplicator * Math.sqrt(width));
		if (scrolling) {
			if (robot != null) {
				if (reverse) {
					robot.mouseWheel(((int) (tcur.getYSpeed() * scrollSpeed)) * -1);
				} else {
					robot.mouseWheel((int) (tcur.getYSpeed() * scrollSpeed));
				}
			}
		} else {

			if (xpos < 0)
				xpos = 0;
			if (xpos > width)
				xpos = width;
			int ypos = pos.y + (int) Math.round(tcur.getYSpeed() * speedMultiplicator * Math.sqrt(height));
			if (ypos < 0)
				ypos = 0;
			if (ypos > height)
				ypos = height;
			if (mouse == tcur.getSessionID()) {
				if (robot != null)
					robot.mouseMove(xpos, ypos);
			}
		}
	}

	public void removeTuioCursor(TuioCursor tcur) {
		if (mouse == tcur.getSessionID()) {
			mouse = -1;
		} else {
			if (robot != null)
				robot.mouseRelease(InputEvent.BUTTON1_MASK);
		}
	}

	public void start() {
		try {
			robot = new Robot();
		} catch (Exception e) {
			System.out.println("failed to initialize mouse robot");
			System.exit(0);
		}

		width = (int) Toolkit.getDefaultToolkit().getScreenSize().getWidth();
		height = (int) Toolkit.getDefaultToolkit().getScreenSize().getHeight();
	}

	public TuioTouchpad() {
		System.out.println("Trackpad started ");
		try {
			robot = new Robot();
			System.out.println("robot started");
		} catch (Exception e) {
			System.out.println("failed to initialize mouse robot");
			System.exit(0);
		}

		width = (int) Toolkit.getDefaultToolkit().getScreenSize().getWidth();
		height = (int) Toolkit.getDefaultToolkit().getScreenSize().getHeight();
	}

	public void startTrackpad(int port) {

		trackpad = new TuioTouchpad();
		client = new TuioClient(port);

		System.out.println("listening for TUIO messages on port " + port);
		client.addTuioListener(trackpad);
		client.connect();
	}

	public void stopTrackpad() {
		client.disconnect();
		System.out.println("Mouse service stopped.");
	}

	public static void main(String argv[]) {

		int port = 3333;

		if (argv.length == 1) {
			try {
				port = Integer.parseInt(argv[1]);
			} catch (Exception e) {
				System.out.println("usage: java TuioTouchpad [port]");
			}
		}

		trackpad = new TuioTouchpad();
		client = new TuioClient(port);

		System.out.println("listening for TUIO messages on port " + port);
		client.addTuioListener(trackpad);
		client.connect();
	}

	public void updateSpeed(int speed) {
		speedMultiplicator = (float) (speed / 100.0);
	}

	public void updateUrls(String firsturl, String secondurl) {
		url1 = firsturl;
		url2 = secondurl;
	}

	public void updateScrollSpeed(int scrollSpeedValue) {
		scrollSpeed = scrollSpeedValue;
	}

	public void updateScrollDirection(boolean selected) {
		reverse = selected;
	}

	@Override
	public void addTuioBlob(TuioBlob tblb) {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateTuioBlob(TuioBlob tblb) {
		// TODO Auto-generated method stub

	}

	@Override
	public void removeTuioBlob(TuioBlob tblb) {
		// TODO Auto-generated method stub

	}

	public void addTuioObject(TuioObject tobj) {
	}

	public void updateTuioObject(TuioObject tobj) {
	}

	public void removeTuioObject(TuioObject tobj) {
	}

	public void refresh(TuioTime bundleTime) {

	}
}
