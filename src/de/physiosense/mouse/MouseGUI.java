/*******************************************************************
 * Copyright (c) 2018 physiosense GmbH
 * 
 * This project or parts of it can not be copied and/or distributed 
 * without the express permission of physiosense GmbH
 * 
 * Proprietary and confidential
 * Written by Philipp Arnolds <arnolds@physiosense.de>, April 2018
 *******************************************************************/

package de.physiosense.mouse;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.WindowEvent;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class MouseGUI extends JFrame {

	private static final long serialVersionUID = 1L;
	private static TuioTouchpad foo = new TuioTouchpad();;
	private static int mouseSpeedValue = 100;
	private static int scrollSpeedValue = 5;
	private static JButton startBtn;
	private static JButton stopBtn;
	private static JTextField url1input;
	private static JTextField url2input;
	private static JSlider mouseSpeedslider;
	private static JSlider scrollSpeedslider;
	private static JCheckBox inverseCheckbox;

	public static void main(String[] args) {
		JFrame mainFrame = new JFrame("physiosense Mouse Control 0.1 beta");
		mainFrame.setSize(400, 320);
		mainFrame.setResizable(false);
		URL iconURL = MouseGUI.class.getResource("images/48icon.png");
		ImageIcon icon = new ImageIcon(iconURL);
		mainFrame.setIconImage(icon.getImage());
		mainFrame.addWindowListener(new java.awt.event.WindowAdapter() {
			public void windowClosing(WindowEvent winEvt) {
				try {
					stopTrackpad();
				} catch (Exception e) {
					// TODO: handle exception
				}
				System.exit(0);
			}
		});

		// Menu bar
		JMenuBar bar = new JMenuBar();
		JMenu menu = new JMenu("Menu");
		bar.add(menu);
		JMenuItem quitAppitem = new JMenuItem("Quit");
		JMenuItem aboutItem = new JMenuItem("About");
		JMenuItem settingsItem = new JMenuItem("Advance Settings");
		menu.add(aboutItem);
		menu.add(settingsItem);
		menu.add(quitAppitem);
		aboutItem.addActionListener(e -> showAboutDialog());
		settingsItem.addActionListener(e -> showSettingsDialog());
		quitAppitem.addActionListener(e -> System.exit(0));

		JPanel panel = new JPanel();

		startBtn = new JButton("Start Trackpad");
		stopBtn = new JButton("Stop Trackpad");
		stopBtn.setEnabled(false);
		startBtn.setPreferredSize(new Dimension(180, 50));
		stopBtn.setPreferredSize(new Dimension(180, 50));
		panel.add(startBtn);
		panel.add(stopBtn);
		startBtn.addActionListener(e -> startTrackpad());
		stopBtn.addActionListener(e -> stopTrackpad());

		// Create mouse speed label.
		JLabel mouseSliderLabel = new JLabel("Mouse Speed", JLabel.CENTER);
		mouseSliderLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		mouseSliderLabel.setPreferredSize(new Dimension(80, 30));

		mouseSpeedslider = new JSlider(NORMAL, 50, 300, 100);
		mouseSpeedslider.setPreferredSize(new Dimension(290, 45));
		mouseSpeedslider.setMajorTickSpacing(50);
		mouseSpeedslider.setMinorTickSpacing(10);
		mouseSpeedslider.createStandardLabels(1);
		mouseSpeedslider.setPaintTicks(true);
		mouseSpeedslider.setPaintLabels(true);
		mouseSpeedslider.addChangeListener(new ChangeListener() {

			public void stateChanged(ChangeEvent event) {
				mouseSpeedValue = mouseSpeedslider.getValue();
				foo.updateSpeed(mouseSpeedValue);
			}
		});

		// Create scroll speed label.
		JLabel scrollSliderLabel = new JLabel("Scroll Speed", JLabel.CENTER);
		scrollSliderLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		scrollSliderLabel.setPreferredSize(new Dimension(80, 30));

		scrollSpeedslider = new JSlider(NORMAL, 1, 20, 5);
		scrollSpeedslider.setPreferredSize(new Dimension(290, 45));
		scrollSpeedslider.setMajorTickSpacing(5);
		scrollSpeedslider.setMinorTickSpacing(1);
		scrollSpeedslider.createStandardLabels(1);
		scrollSpeedslider.setPaintTicks(true);
		scrollSpeedslider.setPaintLabels(true);
		scrollSpeedslider.addChangeListener(new ChangeListener() {

			public void stateChanged(ChangeEvent event) {
				scrollSpeedValue = scrollSpeedslider.getValue();
				foo.updateScrollSpeed(scrollSpeedValue);
			}
		});

		// inverse scrolling
		inverseCheckbox = new JCheckBox("Reverse scroll direction");
		inverseCheckbox.setSelected(false);
		inverseCheckbox.setAlignmentX(LEFT_ALIGNMENT);
		inverseCheckbox.setPreferredSize(new Dimension(350, 30));
		inverseCheckbox.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent e) {
				foo.updateScrollDirection(inverseCheckbox.isSelected());

			}
		});

		panel.add(mouseSliderLabel);
		panel.add(mouseSpeedslider);

		panel.add(scrollSliderLabel);
		panel.add(scrollSpeedslider);
		panel.add(inverseCheckbox);

		// Create url1 label.
		JLabel url1Label = new JLabel("Left URL", JLabel.CENTER);
		url1Label.setAlignmentX(Component.LEFT_ALIGNMENT);
		url1Label.setPreferredSize(new Dimension(60, 30));
		panel.add(url1Label);

		url1input = new JTextField();
		url1input.setText("http://physiosense.de");
		url1input.setPreferredSize(new Dimension(300, 24));
		panel.add(url1input);

		// Create url1 label.
		JLabel url2Label = new JLabel("Right URL", JLabel.CENTER);
		url2Label.setAlignmentX(Component.LEFT_ALIGNMENT);
		url2Label.setPreferredSize(new Dimension(60, 30));
		panel.add(url2Label);

		url2input = new JTextField();
		url2input.setText("https://aachen.digital/");
		url2input.setPreferredSize(new Dimension(300, 30));
		panel.add(url2input);

		// Menu
		mainFrame.setJMenuBar(bar);
		mainFrame.add(panel);
		mainFrame.setVisible(true);
	}

	private static void showSettingsDialog() {
		JDialog settingsDialog = new JDialog();
		settingsDialog.setTitle("Advance Settings");
		settingsDialog.setSize(400, 300);
		settingsDialog.setLocationRelativeTo(null);
		settingsDialog.setModal(true);

		JPanel settingspanel = new JPanel();

		JLabel ipLabel = new JLabel("IP:");
		JLabel portLabel = new JLabel("Port:");

		settingsDialog.add(settingspanel);
		JTextField ipTextField = new JTextField("127.0.0.1");
		JTextField portTextField = new JTextField("3333");
		ipTextField.setSize(200, 30);
		portTextField.setSize(200, 30);
		ipTextField.setEditable(false);
		portTextField.setEditable(false);

		settingspanel.add(ipLabel);
		settingspanel.add(ipTextField);
		settingspanel.add(portLabel);
		settingspanel.add(portTextField);
		settingspanel.setVisible(true);
		settingsDialog.setVisible(true);

	}

	private static void showAboutDialog() {
		// About dialog
		JDialog aboutDialog = new JDialog();
		aboutDialog.setTitle("About");
		aboutDialog.setSize(400, 300);
		aboutDialog.setLocationRelativeTo(null);
		aboutDialog.setModal(true);
		JLabel versionLabel = new JLabel(
				"<html><body>physiosense Mouse client<br>Version 0.1 beta<br>Copyright 2018 physiosense GmbH</body></html>",
				JLabel.CENTER);
		versionLabel.setVerticalAlignment(JLabel.CENTER);
		versionLabel.setHorizontalTextPosition(JLabel.LEFT);
		aboutDialog.add(versionLabel);
		aboutDialog.setVisible(true);
	}

	private static Object stopTrackpad() {
		if (foo != null)
			foo.stopTrackpad();

		startBtn.setEnabled(true);
		stopBtn.setEnabled(false);
		mouseSpeedslider.setEnabled(true);
		url1input.setEditable(true);
		url2input.setEditable(true);
		scrollSpeedslider.setEnabled(true);
		inverseCheckbox.setEnabled(true);

		return foo;
	}

	private static Object startTrackpad() {
		stopBtn.setEnabled(true);
		startBtn.setEnabled(false);
		mouseSpeedslider.setEnabled(false);
		url1input.setEditable(false);
		url2input.setEditable(false);
		scrollSpeedslider.setEnabled(false);
		inverseCheckbox.setEnabled(false);
		foo.updateUrls(url1input.getText(), url2input.getText());
		foo.updateSpeed(mouseSpeedValue);
		foo.updateScrollSpeed(scrollSpeedValue);
		foo.startTrackpad(3333);

		return foo;
	}
}
