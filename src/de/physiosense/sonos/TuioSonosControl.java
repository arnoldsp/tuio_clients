package de.physiosense.sonos;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import tuio.TuioBlob;
import tuio.TuioCursor;
import tuio.TuioListener;
import tuio.TuioObject;
import tuio.TuioTime;

public class TuioSonosControl implements TuioListener {

	private float curserCount = -1;
	private boolean dualTouch;
	private TuioCursor secondCursor;

	public void startSonosControl() {

	}

	public void stopSonosControl() {
	}

	@Override
	public void addTuioCursor(TuioCursor tcur) {
		if (curserCount < 0) {
			curserCount = tcur.getSessionID();
			dualTouch = false;
			System.out.println("singleTouch added...");
//			if(tcur.getY()>0.9) {
//				play();
//			}else {
//				pause();
//			}
			
			if (tcur.getY() > 0.9) {
				// trigger left click
				if (tcur.getX() <= 0.5) {
					System.out.println("play");
					pause();
					// trigger right click
				} else if (tcur.getX() > 0.5) {
					System.out.println("pause");
					play();
				}
			}
		} else {
			System.out.println("dualtouch added...");
			// two finger gesture
			dualTouch = true;
			secondCursor = tcur;
		}
	}

	@Override
	public void updateTuioCursor(TuioCursor tcur) {
		
		if (dualTouch) {
			System.out.println("second id:" + secondCursor.getSessionID());
			
		} else {
			System.out.println("singleTouch gesture");
		}
	}

	@Override
	public void removeTuioCursor(TuioCursor tcur) {
		if (curserCount == tcur.getSessionID()) {
			curserCount = -1;
			System.out.println("cursor removed");
		} else {
			dualTouch = false;
		}
	}

	@Override
	public void addTuioBlob(TuioBlob tblb) {
	}

	@Override
	public void refresh(TuioTime ftime) {
	}

	@Override
	public void updateTuioBlob(TuioBlob tblb) {
	}

	@Override
	public void removeTuioBlob(TuioBlob tblb) {
	}

	@Override
	public void addTuioObject(TuioObject tobj) {
		System.out.println("ubndfuikjbsdf�kubsd�kfub");
	}

	@Override
	public void updateTuioObject(TuioObject tobj) {
	}

	@Override
	public void removeTuioObject(TuioObject tobj) {
	}
	
	private static Object play() {

		String url = "http://localhost:5005/B�ro/play";
		// String url = "http://localhost:5005/zones";
		
		
		String USER_AGENT = "Mozilla/5.0";
		try {

			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();

			// optional default is GET
			con.setRequestMethod("GET");

			// add request header
			con.setRequestProperty("User-Agent", USER_AGENT);

			int responseCode = con.getResponseCode();
			System.out.println("\nSending 'GET' request to URL : " + url);
			System.out.println("Response Code : " + responseCode);

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();

			// print result
			System.out.println(response.toString());

		} catch (Exception e) {
			System.out.println(e.toString());
			// TODO: handle exception
		}
		return null;
	}
	
	private static Object pause() {

		String url = "http://localhost:5005/B�ro/pause";
		String USER_AGENT = "Mozilla/5.0";
		try {

			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();

			// optional default is GET
			con.setRequestMethod("GET");

			// add request header
			con.setRequestProperty("User-Agent", USER_AGENT);

			int responseCode = con.getResponseCode();
			System.out.println("\nSending 'GET' request to URL : " + url);
			System.out.println("Response Code : " + responseCode);

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();

			// print result
			System.out.println(response.toString());

		} catch (Exception e) {
			System.out.println(e.toString());
			// TODO: handle exception
		}

		return null;
	}
}