/*******************************************************************
 * Copyright (c) 2018 physiosense GmbH
 * 
 * This project or parts of it can not be copied and/or distributed 
 * without the express permission of physiosense GmbH
 * 
 * Proprietary and confidential
 * Written by Philipp Arnolds <arnolds@physiosense.de>, April 2018
 *******************************************************************/

package de.physiosense.sonos;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;

import org.json.JSONObject;

import de.physiosense.mouse.MouseGUI;
import tuio.TuioClient;

public class SonosControl extends javax.swing.JFrame {

	private static TuioSonosControl foo = new TuioSonosControl();;
	private static final long serialVersionUID = 1L;
	private static JButton startBtn;
	private static JButton stopBtn;
	private static JButton playBtn;
	private static JButton pauseBtn;
	private static JButton getZonesBtn;
	private static JTextField sonosNameField;
	private static String deviceName = "B�ro";
	private static JButton startTuioBtn;
	private static TuioSonosControl sonos;
	private static TuioClient client;
	private static int port = 3333;

	public static void main(String[] args) {
		JFrame mainFrame = new JFrame("physiosense Sonos Control");
		mainFrame.setSize(400, 300);
		URL iconURL = MouseGUI.class.getResource("images/48icon.png");
		ImageIcon icon = new ImageIcon(iconURL);
		mainFrame.setIconImage(icon.getImage());
		mainFrame.addWindowListener(new java.awt.event.WindowAdapter() {
			public void windowClosing(WindowEvent winEvt) {
				try {
					stopControl();
				} catch (Exception e) {
					// TODO: handle exception
				}
				System.exit(0);
			}
		});

		// Menu bar
		JMenuBar bar = new JMenuBar();
		JMenu menu = new JMenu("Menu");
		bar.add(menu);
		JMenuItem quitAppitem = new JMenuItem("Quit");
		JMenuItem aboutItem = new JMenuItem("About");
		JMenuItem settingsItem = new JMenuItem("Advanced Settings");
		menu.add(aboutItem);
		menu.add(settingsItem);
		menu.add(quitAppitem);
		aboutItem.addActionListener(e -> showAboutDialog());
		settingsItem.addActionListener(e -> showSettingsDialog());
		quitAppitem.addActionListener(e -> System.exit(0));

		JPanel panel = new JPanel();

		startBtn = new JButton("Start Sonos Server");
		stopBtn = new JButton("Stop Sonos Server");
		stopBtn.setEnabled(true);
		startBtn.setPreferredSize(new Dimension(180, 30));
		stopBtn.setPreferredSize(new Dimension(180, 30));

		playBtn = new JButton("Play");
		playBtn.setPreferredSize(new Dimension(180, 30));

		pauseBtn = new JButton("Pause");
		pauseBtn.setPreferredSize(new Dimension(180, 30));

		getZonesBtn = new JButton("Get Devices");
		getZonesBtn.setPreferredSize(new Dimension(180, 30));

		startTuioBtn = new JButton("Start Tuio Client");
		startTuioBtn.setPreferredSize(new Dimension(180, 30));

		panel.add(startBtn);
		panel.add(stopBtn);
		panel.add(playBtn);
		panel.add(pauseBtn);
		panel.add(getZonesBtn);
		panel.add(startTuioBtn);

		startBtn.addActionListener(e -> startControl());
		stopBtn.addActionListener(e -> stopControl());
		playBtn.addActionListener(e -> play());
		pauseBtn.addActionListener(e -> pause());
		getZonesBtn.addActionListener(e -> getZones());
		startTuioBtn.addActionListener(e -> startTuio());

		// Create url1 label.
		JLabel sonosNameLabel = new JLabel("Sonos Name", JLabel.CENTER);
		sonosNameLabel.setAlignmentX(Component.LEFT_ALIGNMENT);
		sonosNameLabel.setPreferredSize(new Dimension(80, 50));
		panel.add(sonosNameLabel);

		sonosNameField = new JTextField();
		sonosNameField.setText("B�ro");
		sonosNameField.setPreferredSize(new Dimension(240, 24));
		panel.add(sonosNameField);

		// Menu
		mainFrame.setJMenuBar(bar);
		mainFrame.add(panel);
		mainFrame.setVisible(true);
	}

	private static Object startTuio() {
		sonos = new TuioSonosControl();
		client = new TuioClient(port);

		System.out.println("listening for TUIO messages on port " + port);
		client.addTuioListener(sonos);
		client.connect();
		return client;
	}

	private static Object getZones() {
		String url = "http://localhost:5005/zones";
		String USER_AGENT = "Mozilla/5.0";
		try {

			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();

			// optional default is GET
			con.setRequestMethod("GET");

			// add request header
			con.setRequestProperty("User-Agent", USER_AGENT);

			int responseCode = con.getResponseCode();
			System.out.println("\nSending 'GET' request to URL : " + url);
			System.out.println("Response Code : " + responseCode);

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream(), StandardCharsets.UTF_8));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();

			System.out.println(response);
			response.deleteCharAt(0);
			response.deleteCharAt(response.length() - 1);
			System.out.println(response);
			JSONObject jObj = new JSONObject(response.toString());

			System.out.println(jObj);
			deviceName = jObj.getJSONObject("coordinator").getString("roomName");
			System.out.println("device Name:" + deviceName);
			// print result
			// System.out.println(response.toString());

		} catch (Exception e) {
			System.out.println(e.toString());
			// TODO: handle exception
		}
		return null;
	}

	private static Object pause() {

		String url = "http://localhost:5005/B�ro/pause";
		String USER_AGENT = "Mozilla/5.0";
		try {

			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();

			// optional default is GET
			con.setRequestMethod("GET");

			// add request header
			con.setRequestProperty("User-Agent", USER_AGENT);

			int responseCode = con.getResponseCode();
			System.out.println("\nSending 'GET' request to URL : " + url);
			System.out.println("Response Code : " + responseCode);

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();

			// print result
			System.out.println(response.toString());

		} catch (Exception e) {
			System.out.println(e.toString());
			// TODO: handle exception
		}

		return null;
	}

	private static Object play() {

		String url = "http://localhost:5005/B�ro/play";
		// String url = "http://localhost:5005/zones";
		String USER_AGENT = "Mozilla/5.0";
		try {

			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();

			// optional default is GET
			con.setRequestMethod("GET");

			// add request header
			con.setRequestProperty("User-Agent", USER_AGENT);

			int responseCode = con.getResponseCode();
			System.out.println("\nSending 'GET' request to URL : " + url);
			System.out.println("Response Code : " + responseCode);

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();

			// print result
			System.out.println(response.toString());

		} catch (Exception e) {
			System.out.println(e.toString());
			// TODO: handle exception
		}
		return null;
	}

	private static void showSettingsDialog() {
		// About dialog
		JDialog settingsDialog = new JDialog();
		// Titel wird gesetzt
		settingsDialog.setTitle("Advance Settings");
		settingsDialog.setSize(400, 300);
		settingsDialog.setLocationRelativeTo(null);
		settingsDialog.setModal(true);

		JPanel settingspanel = new JPanel();

		settingspanel.setVisible(true);
		settingsDialog.setVisible(true);

	}

	private static void showAboutDialog() {
		// About dialog
		JDialog aboutDialog = new JDialog();
		aboutDialog.setTitle("About");
		aboutDialog.setSize(400, 300);
		aboutDialog.setLocationRelativeTo(null);
		aboutDialog.setModal(true);
		JLabel versionLabel = new JLabel(
				"<html><body>physiosense Sonos Control client<br>Version 0.1<br>Copyright 2018 physiosense GmbH</body></html>",
				JLabel.CENTER);
		versionLabel.setVerticalAlignment(JLabel.CENTER);
		versionLabel.setHorizontalTextPosition(JLabel.LEFT);
		aboutDialog.add(versionLabel);
		aboutDialog.setVisible(true);
	}

	/*
	 * output the comandline feedback
	 */
	private static void watch(final Process process) {
		new Thread() {
			public void run() {
				BufferedReader input = new BufferedReader(new InputStreamReader(process.getInputStream()));
				String line = null;
				try {
					while ((line = input.readLine()) != null) {
						System.out.println(line);
						if (line.contains("listening")) {
							System.out.println("Sonos Server started");
						}
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}.start();
	}

	/*
	 * start the Sonos server
	 */
	private static Object startControl() {
		try {
			ProcessBuilder builder = new ProcessBuilder("cmd", "/C", "npm", "start");
			String sonosServerPath = System.getProperty("user.dir");
			sonosServerPath = sonosServerPath + "\\sonosApi";

			System.out.println(sonosServerPath);

			builder.directory(new File(sonosServerPath));
			builder.redirectErrorStream(true);
			final Process process = builder.start();

			// Watch the process
			watch(process);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return null;
	}

	/*
	 * kill the server via terminal
	 */
	private static Object stopControl() {
		try {
			ProcessBuilder builder = new ProcessBuilder("cmd", "/C", "taskkill", "/f", "/im", "node.exe");
			String sonosServerPath = System.getProperty("user.dir");
			sonosServerPath = sonosServerPath + "\\sonosApi";
			builder.directory(new File(sonosServerPath));
			builder.redirectErrorStream(true);
			final Process process = builder.start();

			// Watch the process
			watch(process);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return null;
	}
}
